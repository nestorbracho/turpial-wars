/**
 * Created by turpial on 13-07-16.
 */

$(function () {
    $('[rol="popover"]').popover({
        trigger: "hover", 
        placement: 'bottom',
        toggle : "popover",
        html:true,
        container: 'body',
        template: '<div class="popover popover-medium"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title"></h3><ul class="list-group popover-content"></ul></div></div>',
    }).on('shown.bs.popover', function () {
        $('span.stars').stars();
    });

    // Alpha

    $(".jorge-stats").attr('data-content',
        '<li class="list-group-item text-center">' +
            'Resistencia' +
            '<span class="stars">4.75</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Agilidad' +
            '<span class="stars">4.3</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Punteria' +
            '<span class="stars">3.5</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Agresividad' +
            '<span class="stars">2.5</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Experiencia' +
            '<span class="stars">1.75</span>' +
        '</li>'
    );

    $(".toto-stats").attr('data-content',
        '<li class="list-group-item text-center">' +
            'Resistencia' +
            '<span class="stars">3.5</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Agilidad' +
            '<span class="stars">3.666</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Punteria' +
            '<span class="stars">4.666</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Agresividad' +
            '<span class="stars">2.666</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Experiencia' +
            '<span class="stars">2</span>' +
        '</li>'
    );

    $(".andreina-stats").attr('data-content',
        '<li class="list-group-item text-center">' +
            'Resistencia' +
            '<span class="stars">2.5</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Agilidad' +
            '<span class="stars">2.5</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Punteria' +
            '<span class="stars">2</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Agresividad' +
            '<span class="stars">5</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Experiencia' +
            '<span class="stars">1.75</span>' +
        '</li>'
    );

    $(".jacobo-stats").attr('data-content',
        '<li class="list-group-item text-center">' +
            'Resistencia' +
            '<span class="stars">3.5</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Agilidad' +
            '<span class="stars">3</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Punteria' +
            '<span class="stars">2</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Agresividad' +
            '<span class="stars">2</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Experiencia' +
            '<span class="stars">1</span>' +
        '</li>'
    );

    // Bravo

    $(".ottati-stats").attr('data-content',
        '<li class="list-group-item text-center">' +
            'Resistencia' +
            '<span class="stars">2</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Agilidad' +
            '<span class="stars">4</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Punteria' +
            '<span class="stars">5</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Agresividad' +
            '<span class="stars">4</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Experiencia' +
            '<span class="stars">5</span>' +
        '</li>'
    );

    $(".freddy-stats").attr('data-content',
        '<li class="list-group-item text-center">' +
            'Resistencia' +
            '<span class="stars">3.16</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Agilidad' +
            '<span class="stars">3</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Punteria' +
            '<span>?</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Agresividad' +
            '<span class="stars">0.5</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Experiencia' +
            '<span class="stars">1</span>' +
        '</li>'
    );

    $(".paola-stats").attr('data-content',
        '<li class="list-group-item text-center">' +
            'Resistencia' +
            '<span class="stars">3</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Agilidad' +
            '<span class="stars">2</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Punteria' +
            '<span class="stars">2.5</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Agresividad' +
            '<span class="stars">2</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Experiencia' +
            '<span class="stars">1</span>' +
        '</li>'
    );

    $(".castillo-stats").attr('data-content',
        '<li class="list-group-item text-center">' +
            'Resistencia' +
            '<span class="stars">1.333</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Agilidad' +
            '<span class="stars">2</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Punteria' +
            '<span class="stars">4.333</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Agresividad' +
            '<span class="stars">2.333</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Experiencia' +
            '<span class="stars">4.666</span>' +
        '</li>'
    );

    // Charlie

    $(".leonardo-stats").attr('data-content',
        '<li class="list-group-item text-center">' +
            'Resistencia' +
            '<span class="stars">4</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Agilidad' +
            '<span class="stars">3.666</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Punteria' +
            '<span class="stars">4.666</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Agresividad' +
            '<span class="stars">4</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Experiencia' +
            '<span class="stars">4.666</span>' +
        '</li>'
    );

    $(".guzman-stats").attr('data-content',
        '<li class="list-group-item text-center">' +
            'Resistencia' +
            '<span class="stars">4</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Agilidad' +
            '<span class="stars">1.5</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Punteria' +
            '<span class="stars">1</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Agresividad' +
            '<span class="stars">0</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Experiencia' +
            '<span class="stars">0</span>' +
        '</li>'
    );

    $(".marjorie-stats").attr('data-content',
        '<li class="list-group-item text-center">' +
            'Resistencia' +
            '<span class="stars">2.8</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Agilidad' +
            '<span class="stars">3</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Punteria' +
            '<span class="stars">1.5</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Agresividad' +
            '<span class="stars">5</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Experiencia' +
            '<span class="stars">0</span>' +
        '</li>'
    );

    $(".azpurua-stats").attr('data-content',
        '<li class="list-group-item text-center">' +
            'Resistencia' +
            '<span class="stars">3.5</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Agilidad' +
            '<span class="stars">4.3</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Punteria' +
            '<span class="stars">2</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Agresividad' +
            '<span class="stars">2.2</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Experiencia' +
            '<span class="stars">1</span>' +
        '</li>'
    );

    // Delta

    $(".ricardo-stats").attr('data-content',
        '<li class="list-group-item text-center">' +
            'Resistencia' +
            '<span class="stars">4.83</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Agilidad' +
            '<span class="stars">2.833</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Punteria' +
            '<span class="stars">2.166</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Agresividad' +
            '<span class="stars">2.166</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Experiencia' +
            '<span class="stars">0.3</span>' +
        '</li>'
    );

    $(".vanessa-stats").attr('data-content',
        '<li class="list-group-item text-center">' +
            'Resistencia' +
            '<span class="stars">3.833</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Agilidad' +
            '<span class="stars">3</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Punteria' +
            '<span class="stars">2.3</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Agresividad' +
            '<span class="stars">5</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Experiencia' +
            '<span class="stars">0</span>' +
        '</li>'
    );

    // Echo

    $(".nelson-stats").attr('data-content',
        '<li class="list-group-item text-center">' +
            'Resistencia' +
            '<span class="stars">4</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Agilidad' +
            '<span class="stars">2</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Punteria' +
            '<span class="stars">2</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Agresividad' +
            '<span class="stars">3</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Experiencia' +
            '<span class="stars">2</span>' +
        '</li>'
    );

    $(".francisco-stats").attr('data-content',
        '<li class="list-group-item text-center">' +
            'Resistencia' +
            '<span class="stars">2</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Agilidad' +
            '<span class="stars">3</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Punteria' +
            '<span>?</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Agresividad' +
            '<span class="stars">1</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Experiencia' +
            '<span class="stars">0</span>' +
        '</li>'
    );

    $(".steff-stats").attr('data-content',
        '<li class="list-group-item text-center">' +
            'Resistencia' +
            '<span class="stars">2</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Agilidad' +
            '<span class="stars">3</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Punteria' +
            '<span class="stars">1</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Agresividad' +
            '<span class="stars">5</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Experiencia' +
            '<span class="stars">0</span>' +
        '</li>'
    );

    $(".pedro-stats").attr('data-content',
        '<li class="list-group-item text-center">' +
            'Resistencia' +
            '<span class="stars">2</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Agilidad' +
            '<span class="stars">3</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Punteria' +
            '<span class="stars">5</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Agresividad' +
            '<span class="stars">2</span>' +
        '</li>' +
        '<li class="list-group-item text-center">' +
            'Experiencia' +
            '<span class="stars">0</span>' +
        '</li>'
    );

    $('span.stars').stars();
});

$.fn.stars = function() {
    return $(this).each(function() {
        $(this).html($('<span />').width(Math.max(0, (Math.min(5, parseFloat($(this).html())))) * 16));
    });
};

$(".count-down").countdown("2016/07/16 08:00:00", function(event) {
    $(this).text(
        event.strftime('%D dias %H:%M:%S')
    );
});